public class Binary {

    public Binary() {
    }

    public String convertToBinary(long n) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; n > 0 ; n /= 2) {
            if (n % 2 == 0) {
                stringBuilder.append(0);
            } else {
                stringBuilder.append(1);
            }
        }

        return stringBuilder.reverse().toString();
    }
}

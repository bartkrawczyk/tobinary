import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        long n;

        Scanner reader = new Scanner(System.in);

        System.out.println("Give a number which you want to convert to binary:");
        n = reader.nextLong();
        reader.close();

        Binary binary = new Binary();

        System.out.println(binary.convertToBinary(n));
    }
}
